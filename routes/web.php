<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/users', [App\Http\Controllers\UserController::class, 'index'])->name('allusers');
Route::get('create', [App\Http\Controllers\UserController::class, 'create'])->name('createuser');
Route::post('store', [App\Http\Controllers\UserController::class, 'store'])->name('storeuser');
Route::get('edit/{id}', [App\Http\Controllers\UserController::class, 'edit'])->name('edituser');
Route::put('update/{id}', [App\Http\Controllers\UserController::class, 'update'])->name('updateuser');
Route::delete('delete', [App\Http\Controllers\UserController::class, 'destroy'])->name('deleteuser');


