1. First Commit
   created laravel project with composer
2. Second Commit ( Auth )
   run commands
   composer require laravel/ui,
   php artisan ui bootstrap --auth,
   (npm install,npm run dev) 2 times,
   php artisan migrate
3. Third Commit (Used Factory and seeder for user table)
   changes in Databaseseeder.php
   run commands
   php artisan db:seed
4. Fourth Commit
   run commands
   composer require yajra/laravel-datatables-oracle:"~9.0"
   added Yajra\DataTables\DataTablesServiceProvider::class, in
   config/app.php providers
   run commands
   php artisan vendor:publish --tag=datatables
   php artisan make:controller UserController -r
   added views and added logic for crud of users table
5. Fifth Commit
   Created User Observer
   sent mail on user delete
   accessor and mutator
